import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { Profile } from '../_classes/profile.class';
import { ProfileService } from '../_services/profile/profile.service';


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

    private subscription: Subscription = new Subscription();
    profile: Profile;

    profileForm: FormGroup = new FormGroup({
        firstName: new FormControl(),
        lastName: new FormControl(),
        age: new FormControl(),
        email: new FormControl(),
    });


    constructor(private profileService: ProfileService, private fb: FormBuilder) { }

    ngOnInit() {
        this.getProfile();
        this.profileForm = this.fb.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            age: ['', [Validators.required, Validators.minLength(1)]],
            email: ['', Validators.required]
        });
    }

    getProfile(): void {
        this.subscription.add(
            this.profileService.getProfile().subscribe(profile => {
                this.profile = profile;
                this.profileForm.patchValue({
                    firstName: profile.firstName, 
                    lastName: profile.lastName,
                    age: profile.age,
                    email: profile.email
                });

            })
        );
    }

    submitProfileForm(): void {
        if(this.profileForm.valid){
            this.profile.firstName = this.profileForm.value.firstName;
            this.profile.lastName = this.profileForm.value.lastName;
            this.profile.age = this.profileForm.value.age;
            this.profile.email = this.profileForm.value.email;
            alert('Values changed successfuly');
        }
    }

}
