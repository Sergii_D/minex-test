export class Profile {
    id: number;
    firstName: string;
    lastName: string;
    profilePicture: string;
    age: number;
    email: string;
    hobbies: string[];
    bankAccountId: number[];

    constructor() {

    }
}