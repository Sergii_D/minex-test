export class Account {
    id: number;
    name: string;
    description: string;
    credentials: string;
    transactionIds: number[];

    constructor() {

    }
}