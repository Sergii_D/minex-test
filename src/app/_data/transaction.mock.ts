import { Transaction } from "../_classes/transaction.class";

export const TRANSACTIONS: Transaction[] = [
    {
        id: 0,
        name: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab',
        date: 1502514740699,
        ammount: 50,
    },
    {
        id: 1,
        name: 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account',
        date: 1512515740699,
        ammount: 90,
    },
    {
        id: 2,
        name: 'A vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et',
        date: 1522515740699,
        ammount: 150,
    },
]