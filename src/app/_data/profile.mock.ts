import { Profile } from "../_classes/profile.class";

export const PROFILE: Profile = {
    id: 0,
    firstName: 'John',
    lastName: 'Doe',
    profilePicture: 'https://image.flaticon.com/icons/svg/147/147144.svg',
    age: 42,
    email: '123@dfgssfgs.df',
    hobbies: ['Hobbie1', 'Hobbie2'],
    bankAccountId: [0]
}