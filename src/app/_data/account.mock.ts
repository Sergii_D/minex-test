import { Account } from "../_classes/account.class";

export const ACCOUNTS: Account[] = [{
    id: 0,
    name: 'My first account',
    description: 'These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. ',
    credentials: '5646s5f4sd65f4s6df4',
    transactionIds: [0, 1, 2],
}]