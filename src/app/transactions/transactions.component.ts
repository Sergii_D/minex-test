import { Component, OnInit, Input } from '@angular/core';
import { Transaction } from '../_classes/transaction.class';

@Component({
    selector: 'app-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

    @Input() transactions: Transaction[];

    constructor() { }

    ngOnInit() {
    }

    getDate(date: number): string {
        return new Date(date).toDateString();
    }

}
