import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

import { TransactionService } from '../_services/transaction/transaction.service';
import { AccountService } from '../_services/account/account.service';
import { Transaction } from '../_classes/transaction.class';
import { Account } from '../_classes/account.class';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    private subscription: Subscription = new Subscription();
    account: Account;
    transactions: Transaction[];

    constructor(private transactionService: TransactionService,
        private accountService: AccountService) { }

    ngOnInit() {
        this.subscription.add(
            this.accountService.getAccount(0).subscribe(account => {
                this.account = account;
            })
        );
        this.subscription.add(
            this.transactionService.getTransactions([0, 1, 2]).subscribe(transactions => {
                this.transactions = transactions;
            })
        );

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
