import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/internal/operators';

import { Transaction } from '../../_classes/transaction.class';
import { TRANSACTIONS } from '../../_data/transaction.mock';
@Injectable({
    providedIn: 'root'
})
export class TransactionService {

    constructor() { }

    getTransactions(transactionIds: number[]): Observable<Transaction[]> {
        let transactions: Transaction[] = TRANSACTIONS.filter(el => transactionIds.indexOf(el.id) >= 0);
        return of(transactions).pipe(delay(2000));
    }
}
