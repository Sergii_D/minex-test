import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/internal/operators';

import { PROFILE } from '../../_data/profile.mock';
import { Profile } from '../../_classes/profile.class';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    constructor() { }

    getProfile(): Observable<Profile> {
        return of(PROFILE).pipe(delay(1500));
    }

}
