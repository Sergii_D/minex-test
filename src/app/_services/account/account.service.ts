import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/internal/operators';

import { ACCOUNTS } from '../../_data/account.mock';
import { Account } from '../../_classes/account.class';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor() { }

  getAccount(accountId: number): Observable<Account> {
    let targetAcc: Account = ACCOUNTS.find(el => el.id === accountId);
    return of(targetAcc).pipe(delay(3000));
}
}
